mkdir build;cd build
unset CPPFLAGS
../configure \
--prefix=/usr \
--enable-gold \
--enable-ld=default \
--enable-shared \
--disable-werror \
--enable-64-bit-bfd \
--with-system-zlib

make tooldir=/usr
