cd binutils-$surum
	
mkdir build
cd build
# hardened derleme yöntemleri içerdiği için mpsd derleme bayrakları pasife alındı
unset CFLAGS
CC=$ONSISTEM_TARGET-gcc        \
AR=$ONSISTEM_TARGET-ar         \
RANLIB=$ONSISTEM_TARGET-ranlib \
../configure                   \
--prefix=/tools            	   \
--disable-nls                  \
--disable-werror               \
--with-lib-path=/tools/lib     \
--with-sysroot

make 
