mkdir build
cd build
../configure --prefix=/tools \
--with-sysroot=$ONSISTEM_CHROOT \
--with-lib-path=/tools/lib \
--target=$ONSISTEM_TARGET \
--disable-nls \
--disable-werror \
--enable-gprofng=no

make
