sed -i '/MV.*old/d' Makefile.in
sed -i '/{OLDSUFF}/c:' support/shlib-install

CFLAGS="$CFLAGS -fPIC"
./configure --prefix=/tools \
--disable-static

make SHLIB_LIBS="-L/tools/lib -lncursesw"
