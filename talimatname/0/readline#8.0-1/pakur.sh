make SHLIB_LIBS="-L/tools/lib -lncursesw" DESTDIR=$PKG install

chmod -v u+w $PKG/tools/lib/lib{readline,history}.so.*
ln -sfv /tools/lib/libreadline.so.8.0 $PKG/tools/lib/libreadline.so
ln -sfv /tools/lib/libhistory.so.8.0 $PKG/tools/lib/libhistory.so

rm -rf $PKG/tools/bin
rm -rf $PKG/tools/share/info
rm -rf $PKG/tools/share/doc
