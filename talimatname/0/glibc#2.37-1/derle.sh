mkdir -v build
cd build

export M4=m4
../configure                       \
--prefix=/tools                    \
--host=$ONSISTEM_TARGET            \
--build=$(../scripts/config.guess) \
--enable-kernel=4.14                \
--with-headers=/tools/include

make
