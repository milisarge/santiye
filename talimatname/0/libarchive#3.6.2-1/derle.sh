./configure --prefix=/tools \
--without-xml2 \
--without-nettle \
--disable-static \
--without-expat \
--disable-bsdcpio \
--without-iconv \
--without-bz2lib

make
